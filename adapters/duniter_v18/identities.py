import json
import plyvel
from pathlib import Path


class LevelDBIdentitiesRepository:

    DEFAULT_LEVELDB_PATH = "./leveldb"
    DB_INDEX = "level_iindex"

    def __init__(self, leveldb_path: str):
        """
        Init connection

        :param leveldb_path: Path of database
        """
        self.index = plyvel.DB(str(Path(leveldb_path).joinpath(self.DB_INDEX)))

    def __iter__(self):
        """
        Iterate over pubkey: str, identity: dict

        :return:
        """
        for key, value in self.index.__iter__():
            yield key.decode("utf-8"), get_identity_from_db_entry(value)

    def get(self, pubkey: str):
        """
        Return identity dict from pubkey

        :param pubkey: Identity account pubkey
        :return:
        """
        return get_identity_from_db_entry(self.index.get(pubkey.encode("utf-8")))


def get_identity_from_db_entry(json_string: str) -> dict:
    """
    Get identity dict list from json string
    Parse list to get last updated status of identity

    :param json_string: Json entry
    :return:
    """
    identity_list = json.loads(json_string)  # type: list
    # update first identity properties except for None value
    return {key: value for list_item in identity_list for (key, value) in list_item.items() if value is not None or list_item == identity_list[0]}
