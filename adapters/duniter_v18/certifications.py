import json
import plyvel
from pathlib import Path


class LevelDBCertificationsRepository:

    DEFAULT_LEVELDB_PATH = "./leveldb"
    DB_INDEX = "level_cindex"

    def __init__(self, leveldb_path: str):
        """
        Init connection

        :param leveldb_path: Path of database
        """
        self.index = plyvel.DB(str(Path(leveldb_path).joinpath(self.DB_INDEX)))

    def __iter__(self):
        """
        Iterate over pubkey: str, issuer: dict
        (with issued certifications updated to their last state)

        :return:
        """
        for key, value in self.index.__iter__():
            yield key.decode("utf-8"), get_issuer_from_db_entry(value)

    def get(self, pubkey: str) -> dict:
        """
        Return issuer dict from pubkey

        :param pubkey: Identity account pubkey
        :return:q
        """
        db_entry = self.index.get(pubkey.encode("utf-8"))
        return get_issuer_from_db_entry(db_entry)


def get_issuer_from_db_entry(json_string: str) -> dict:
    """
    Get issuer dict from json string
    (with issued certifications updated to their last state)

    :param json_string: Json entry
    :return:
    """
    issuer = json.loads(json_string)
    issuer["issued"] = get_updated_certifications_from_issuer(issuer["issued"])
    return issuer


def get_updated_certifications_from_issuer(issued_certifications: list) -> list:
    """
    Return certifications from issuer_pubkey

    Certifications are updated to the last state for each receiver

    :param issued_certifications: Issuer issued certifications list
    :return:
    """
    issuer_certifications = []
    for cert in issued_certifications:
        if cert["op"] == "CREATE":
            receiver_pubkey = cert["receiver"]
            # filter only certifications to receiver
            issuer_to_receiver_certifications = [issuer_certification for issuer_certification in
                                                 issued_certifications if
                                                 issuer_certification["receiver"] == receiver_pubkey]
            # update first certification properties except for None value
            issuer_to_receiver_certification = {
                key: value for issuer_certification in issuer_to_receiver_certifications for (key, value) in
                issuer_certification.items() if
                value is not None or issuer_certification == issuer_to_receiver_certifications[0]}

            issuer_certifications.append(issuer_to_receiver_certification)

    return issuer_certifications
