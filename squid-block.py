#!/usr/bin/env python3

import json
import os

from lib.functions import get_blocks

DEFAULT_LEVELDB_PATH = "./leveldb"
LEVELDB_PATH = os.getenv("LEVELDB_PATH", DEFAULT_LEVELDB_PATH)


def main():
    # get blocks
    block_hist = get_blocks(LEVELDB_PATH)

    # Dump JSON to file
    print("Exporting...")
    block_hist_json = json.dumps(block_hist, indent=2).encode()
    gtest_json = open("output/block_hist.json", "wb")
    gtest_json.write(block_hist_json)


if __name__ == "__main__":
    print("Prepare blocks for squid")
    main()
    print("Done\n")
