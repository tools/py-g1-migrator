#!/usr/bin/env python3

#  Copyright (C) 2022 Axiom-Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import json
import os
import sys
from time import time

from adapters.duniter_v18.ud_value import LevelDBUDValueRepository
from lib.functions import get_identities_and_wallets

DEFAULT_LEVELDB_PATH = "./leveldb"
LEVELDB_PATH = os.getenv("LEVELDB_PATH", DEFAULT_LEVELDB_PATH)


def main():
    # Get optional arguments
    opt1 = ""
    if len(sys.argv) > 1:
        opt1 = int(sys.argv[1])

    # define start timestamp
    start_timestamp = opt1
    # if not defined set start time to now
    if start_timestamp == "":
        start_timestamp = int(time())

    # Dump ĞTest parameters
    print("    dump ĞTest parameters...")
    # Get last block info
    ud_value_repository = LevelDBUDValueRepository(LEVELDB_PATH)
    last_block = ud_value_repository.get_last()
    ud_value_repository.close()

    initial_monetary_mass = last_block["mass"]
    last_block_time = last_block["medianTime"]

    first_ud_value = last_block["dividend"]
    first_ud_reeval = last_block["udReevalTime"]
    inital_monetary_mass = last_block["mass"]

    # Add identities and wallets
    (identities, other_wallets) = get_identities_and_wallets(start_timestamp, LEVELDB_PATH)

    # Add wallets bloc
    print("    add simple wallets...")

    # Final ĞTest genesis JSON
    genesis = {
        "first_ud_value": first_ud_value,
        "first_ud_reeval": first_ud_reeval,
        "current_block": {
            "number": last_block["number"],
            "medianTime": last_block["medianTime"],
        },
        # "first_ud_time": FIRST_UD_TIME, # this field does not exist in Duniter
        "initial_monetary_mass": inital_monetary_mass,
        "identities": identities,
        "wallets": other_wallets,
        # "treasury": treasury, # would need to modify pallet treasury, adding it as an account instead
    }

    # Dump JSON to file
    genesis_json = json.dumps(genesis, indent=2).encode()
    genesis_fid = open("output/genesis.json", "wb")
    genesis_fid.write(genesis_json)


if __name__ == '__main__':
    print("Generate ĞTest genesis with up-to-date Ğ1 data")
    main()
    print("Done\n")
