import json
import plyvel
from pathlib import Path


class LevelDBBlocksRepository:

    DEFAULT_LEVELDB_PATH = "./leveldb"
    DB_INDEX = "level_blockchain"

    def __init__(self, leveldb_path: str):
        """
        Init connection

        :param leveldb_path: Path of database
        """
        self.index = plyvel.DB(str(Path(leveldb_path).joinpath(self.DB_INDEX)))

    def __iter__(self):
        """
        Iterate over number: int, block: dict

        :return:
        """
        for key, value in self.index.__iter__():
            yield int(key), get_block_from_db_entry(value)

    def get(self, number: int) -> dict:
        """
        Return block dict from number

        :param number: Block number
        :return:
        """
        return get_block_from_db_entry(self.index.get(f"{number:010}".encode()))


def get_block_from_db_entry(json_string: str) -> dict:
    """
    Get block dict from json string

    :param json_string: Json entry
    :return:
    """
    return json.loads(json_string)
