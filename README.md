# Data migration from Ğ1 v1 to Ğ1 v2

## Execution

This program is used in Duniter CI:

https://git.duniter.org/nodes/rust/duniter-v2s/-/blob/56998122e42afd2c2c1642a72a6772a82490ccda/.gitlab-ci.yml#L270-L298

With the docker image produced by Dockerfile:

```sh
docker buildx build . -t h30x/py-g1-migrator
docker image push h30x/py-g1-migrator
```

## Dev

On your Duniter node

```sh
# create temporary directory
mkdir /tmp/backup-g1-duniter-1.8.7
# copy database ~ 1.5 GB
cp -R $HOME/.config/duniter/duniter_default/data /tmp/backup-g1-duniter-1.8.7/
# compress it
tar -cvzf /tmp/backup-g1-duniter-1.8.7.tgz /tmp/backup-g1-duniter-1.8.7
# make it available with http (here it's available with https://files.coinduf.eu/backup-g1-duniter-1.8.7.tgz)
mv /tmp/backup-g1-duniter-1.8.7.tgz /var/www/files.coinduf.eu
```

In your `py-g1-migrator` folder


```sh
# fetch database dump, extract, and move to your input folder
curl https://files.coinduf.eu/backup-g1-duniter-1.8.7.tgz -o inputs/g1-dump.tgz
tar xvzf inputs/g1-dump.tgz  -C inputs
mv inputs/tmp/backup-g1-duniter-1.8.7 inputs/duniter_default

# use a python virtual environment, install requirements, and set env var to tell where the database is
python -m venv env
source ./env/bin/activate
pip install -r requirements.txt
export LEVELDB_PATH="./inputs/duniter_default/data/leveldb" 
# --- MAIN ---
# main script outputs ./output/genesis.json which is used to build Duniter genesis state
./main.py

# --- SQUID ---
# squid scripts are used by Duniter-Squid to provide seamless history for client users
./squid-block.py # ./output/block_hist.json
./squid-cert.py # ./output/cert_hist.json
./squid-tx.py # ./output/tx_hist.json

# make artifacts available to other (this will be done by the CI)
scp ./output/block_hist.json wolf:/var/www/files.coinduf.eu/
scp ./output/tx_hist.json wolf:/var/www/files.coinduf.eu/
scp ./output/cert_hist.json wolf:/var/www/files.coinduf.eu/
```
