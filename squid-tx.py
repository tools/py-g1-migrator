#!/usr/bin/env python3

import json
import os

from lib.functions import get_tx

DEFAULT_LEVELDB_PATH = "./leveldb"
LEVELDB_PATH = os.getenv("LEVELDB_PATH", DEFAULT_LEVELDB_PATH)


def main():
    # get txs
    tx_hist = get_tx(LEVELDB_PATH)

    # Dump JSON to file
    print("Exporting...")
    tx_hist_json = json.dumps(tx_hist, indent=2).encode()
    gtest_json = open("output/tx_hist.json", "wb")
    gtest_json.write(tx_hist_json)


if __name__ == "__main__":
    print("Prepare tx for squid")
    main()
    print("Done\n")
