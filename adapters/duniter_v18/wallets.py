import json
import plyvel
from pathlib import Path


class LevelDBWalletsRepository:

    DEFAULT_LEVELDB_PATH = "./leveldb"
    DB_INDEX = "level_wallet"

    def __init__(self, leveldb_path: str):
        """
        Init connection

        :param leveldb_path: Path of database
        """
        self.index = plyvel.DB(str(Path(leveldb_path).joinpath(self.DB_INDEX)))

    def __iter__(self):
        """
        Iterate over key: str, wallet: dict

        :return:
        """
        for key, value in self.index.__iter__():
            yield key.decode("utf-8"), get_wallet_from_db_entry(value)

    def get(self, unlock_expression: str) -> dict:
        """
        Return wallet dict from unlock_expression

        :param unlock_expression: Account money unlock expression
        :return:
        """
        return get_wallet_from_db_entry(self.index.get(unlock_expression.encode("utf-8")))


def get_wallet_from_db_entry(json_string: str) -> dict:
    """
    Get wallet dict from json string

    :param json_string: Json entry
    :return:
    """
    return json.loads(json_string)
