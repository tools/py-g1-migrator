#!/usr/bin/env python3

import json
import os

from lib.functions import get_cert

DEFAULT_LEVELDB_PATH = "./leveldb"
LEVELDB_PATH = os.getenv("LEVELDB_PATH", DEFAULT_LEVELDB_PATH)


def main():
    # get certs
    cert_hist = get_cert(LEVELDB_PATH)

    # Dump JSON to file
    print("Exporting...")
    cert_hist_json = json.dumps(cert_hist, indent=2).encode()
    gtest_json = open("output/cert_hist.json", "wb")
    gtest_json.write(cert_hist_json)


if __name__ == "__main__":
    print("Prepare cert for squid")
    main()
    print("Done\n")
