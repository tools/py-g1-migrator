import json
import plyvel
from pathlib import Path


class LevelDBUDValueRepository:

    DEFAULT_LEVELDB_PATH = "./leveldb"
    DB_INDEX = "level_bindex"

    def __init__(self, leveldb_path: str):
        """
        Init connection

        :param leveldb_path: Path of database
        """
        self.index = plyvel.DB(str(Path(leveldb_path).joinpath(self.DB_INDEX)))

    def __iter__(self):
        """
        Iterate over number: int, block: dict

        :return:
        """
        for key, value in self.index.__iter__():
            yield int(key), get_block_from_db_entry(value)

    def get(self, number: int) -> dict:
        """
        Return block with UD value dict from number

        :param number: Block number
        :return:
        """
        return get_block_from_db_entry(self.index.get(number))

    def get_last(self) -> dict:
        """
        Get last block with UD value

        :return:
        """
        iterator = self.index.iterator(reverse=True)
        key, value = next(iterator)
        return get_block_from_db_entry(value)

    def close(self):
        """
        Close LevelDB connection to index

        :return:
        """
        self.index.close()


def get_block_from_db_entry(json_string: str) -> dict:
    """
    Get block dict from json string

    :param json_string: Json entry
    :return:
    """
    return json.loads(json_string)
