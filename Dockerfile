# this Dockerfile creates an image used by Duniter CI to run py-g1-migrator
FROM python:3.9.18-bullseye

WORKDIR /app
COPY ./requirements.txt .

# install libleveldb required by plyvel
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y libleveldb-dev

# install python requirements
RUN pip install -r requirements.txt
